﻿#include <iostream>
#include <cassert>
#include <iomanip>
using namespace std;
template <typename T>
class Stack
{
private:
    T* StackStack;
    int size;
    int top;
public:
    Stack(int = 10);
    Stack(const Stack<T>&);
    ~Stack() { delete [] StackStack; }
    void push(const T&);
    void printStack();
    T pop();
};

template <typename T>
Stack<T>::Stack(int maxSize) :
    size(maxSize)
{
    StackStack = new T[size];
    top = -1;
    
   }
template <typename T>
void Stack<T>::push(const T& value)
{
    assert(top < size);

    StackStack[++top] = value;
   
    }
template <typename T>
T Stack<T>::pop()
{
    assert(top >= 0);

    StackStack[--top];

    }

template <typename T>
void Stack<T>::printStack()
{
    for (int ix = top - 1; ix >= 0; ix--)
        cout << "" << setw(4) << StackStack [ix] << endl;
}

int main()
{
    setlocale(0, "Russian");

        Stack<char> stackSymbol(5);
        int ct = 0;
        char ch;

        while (ct++ < 5)
        {
            cin >> ch;
            stackSymbol.push(ch);
        }
   
    stackSymbol.printStack();

    cout << "Привет \n";

}
